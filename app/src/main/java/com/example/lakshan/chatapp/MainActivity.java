package com.example.lakshan.chatapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lakshan.chatapp.stagno.Steg;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.scottyab.aescrypt.AESCrypt;

import java.io.ByteArrayOutputStream;
import java.security.GeneralSecurityException;

public class MainActivity extends AppCompatActivity {

    public static int SIGN_IN_REQUEST_CODE = 1;
    private FirebaseListAdapter<ChatMessage> adapter;
    RelativeLayout activitymain;
    String encryptedMsg = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Snackbar.make(activitymain, "you have been signed out", Snackbar.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (requestCode == RESULT_OK) {
                Snackbar.make(activitymain, "successfull loged", Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(activitymain, "sign in failed,try again", Snackbar.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activitymain = (RelativeLayout) findViewById(R.id.acitivity_main);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = (EditText) findViewById(R.id.input);


                String imageStringtoSend = "";
                //Encrypt the message
                try {
                    encryptedMsg = AESCrypt.encrypt("", input.getText().toString());
                } catch (GeneralSecurityException e) {
                    //handle error
                }

                try {
                    imageStringtoSend = encodedImageAsString(encryptedMsg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference()
                        .push()
                        .setValue(new ChatMessage(imageStringtoSend,
                                FirebaseAuth.getInstance().getCurrentUser().getEmail()));

                // Clear the input
                input.setText("");

            }
        });


        //check it not sign-in then navigate signin page
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().build(), SIGN_IN_REQUEST_CODE);
        } else {
            // Snackbar.make(activitymain,"Welcome ",FirebaseAuth.getInstance().getCurrentUser().getEmail(),Snackbar.LENGTH_SHORT).show();
        }


        displaymessages();


    }

    public void displaymessages() {
        final ListView listOfMessages = (ListView) findViewById(R.id.list_of_messages);

        adapter = new FirebaseListAdapter<ChatMessage>(this, ChatMessage.class,
                R.layout.list_item, FirebaseDatabase.getInstance().getReference()) {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {
                // Get references to the views of message.xml
                //TextView messageText = (TextView)v.findViewById(R.id.message_text);
                TextView messageUser = (TextView) v.findViewById(R.id.message_user);
                TextView messageTime = (TextView) v.findViewById(R.id.message_time);
                ImageView imgView = (ImageView) v.findViewById(R.id.imgView);

                // Set their text
                //messageText.setText(model.getMessageText());
                imgView.setImageBitmap(StringToBitMap(model.getMessageText()));
                messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));


            }
        };

        listOfMessages.setAdapter(adapter);

        listOfMessages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Log.d("POSITION : ", String.valueOf(position));
                String encryptedMessage = adapter.getItem(position).getMessageText();
                //DECRYPTED MESSAGE
                //String messageAfterDecrypt = AESCrypt.decrypt("", encryptedMessage);
                Log.d("AFTER DECRYPT : ", encryptedMessage);

                //Show message in a dialog box
                try {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Secret Message")
                            .setMessage(getHiddenMessage(encryptedMessage))
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void encoding(String hiddenMsg) throws Exception {

        String hiddenMessage = hiddenMsg;

        //Bitmap bitmap = BitmapHelper.createTestBitmap(200, 200);
        //Input image as jpeg from drawable
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cat);

        //encode hidden message to the image
        Bitmap encodedBitmap = Steg.withInput(bitmap).encode(hiddenMessage).intoBitmap();

        //decode the hidden message from image
        String decodedMessage = Steg.withInput(encodedBitmap).decode().intoString();

        Log.d(getClass().getSimpleName(), "Decoded Message: " + decodedMessage);

    }

    // Getting image from the string
    private String getHiddenMessage(String hiddenMsg) throws Exception {

        //decode the hidden message from image
        String decodedMessage = Steg.withInput(StringToBitMap(hiddenMsg)).decode().intoString();

        String messageAfterDecrypt = AESCrypt.decrypt("", decodedMessage);

        Log.d(getClass().getSimpleName(), "Decoded Message: " + messageAfterDecrypt);

        return messageAfterDecrypt;
    }


    // Getting encoded image as a string
    private String encodedImageAsString(String hiddenMsg) throws Exception {

        String hiddenMessage = hiddenMsg;

        //Bitmap bitmap = BitmapHelper.createTestBitmap(200, 200);
        //Input image as jpeg from drawable
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cat);

        //encode hidden message to the image
        Bitmap encodedBitmap = Steg.withInput(bitmap).encode(hiddenMessage).intoBitmap();

        return BitMapToString(encodedBitmap);
    }


    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }


    // Used to display the notifications on top
    //TODO: add to the send button function
    public void showNotification(String msg, String sender) {
        Notification.Builder n = new Notification.Builder(this)
                .setContentTitle("From : " + sender)
                .setContentText(msg)
                .setSmallIcon(R.mipmap.ic_launcher);
        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, n.build());
    }

}
